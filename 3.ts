class V {
    constructor(protected motor: string) { }
}

class C extends V {
    constructor(private m: string) {
        super(m);
    }
    bajarVentanilla() { }
}

class A extends V {
    constructor(private m: string) {
        super(m);
    }
    bajarTrenAterrizaje() { }
}

function creaVehiculos(tipo: string): V {
    let v: V;
    if (tipo === 'coche') {
        v = new C('diesel');
    } else if (tipo === 'avion') {
        v = new A('reaccion');
    }
    return v;
}

let coche = <C>creaVehiculos('coche');
let avion = <A>creaVehiculos('avion');

avion.bajarTrenAterrizaje();
coche.bajarVentanilla();
enum tiposMotor { electrico, gasolina, diesel };

class Motor {
    private tipo: tiposMotor;

    public arranca() {

    }
}

class Coche {
    private motor: Motor | string;
    private automatico: boolean;

    constructor(esAutomatico: boolean, motor?: Motor) {
        this.automatico = esAutomatico;
        if (esAutomatico) {
            this.motor = motor;            
        } else {
            this.motor = 'manual';
        }        
    }

    public desplazarse() {
        if (!this.automatico) {
            (<Motor>this.motor).arranca();
        }
    }
}